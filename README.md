
# to-do-ui

  

<table>

<tr>

<td>

[Vue](https://vuejs.org/) app for listing todo list and adding new todo item

</td>

</tr>

</table>

  
  

## Demo

Here is a working live demo : http://34.125.216.15:1017

## Usage

Write what you plan to do on text box and click add button
  

## SETUP

  

- [ ]  Clone this repo to your desktop and run `npm install` to install all the dependencies.
- [ ]  Run `yarn serve`  for start development server.
  

## Development

This app was developed by adopting TDD.

### File system

#### Dist

Contains build exports

#### Logs

Contains pact logs

#### Pacts

##### interactions.js

Contains back-end interaction definitons for Pact

##### MockServer.js

Contains scripts that run and manage the mock back-end server.

##### todoapp-todoapi.json

Contains made up contract configurations after running Consumer Driven Contract Tests.

#### Src

##### components

Contains unit ui components and helpful modules.

###### Fetch.js
Basic module that performs API calls.

#### Config

File where developers make application configurations like defining global enums, constants, api urls for development or production environment etc.

#### Services

Interfaces for Api Calls. Contains functions for calling easily back-end endpoints

#### Tests

Contains test scripts.


  

## Testing and Deployment
  

## Test and Deploy

-  #### Uses the built-in continuous integration and continuous delivery process in GitLab.
### CI/CD
- #### When pushed code to main branch, will trigger CI/CD pipeline.  The process runs at Gitlab Runner which runs on a remote virtual machine and contains running all test and deploying application. Pipeline configuration code is  located on  .gitlab-ci.yml file.
 #### Pipeline
##### The following steps are applied sequentially:
-	Build App
-	Run unit tests, contract tests on local network
-	Deploy app to test environment
-	Run acceptance test on test environment
-	Deploy app to production
	
 
### Deployment Tools
#### Deployment is by using Docker
	- Connects to the remote virtual machine with Docker installed via ssh in an automated CD process.
	- Pulls this repo to machine and create Docker image from Dockerfile.
	- Runs the container from related image.

### Testing Tools
- [Mocha](https://mochajs.org/)
- [Jest](https://jestjs.io/)
- [Pact](https://pact.io/)
- [Cypress](https://www.cypress.io/)
