import { shallowMount } from '@vue/test-utils';
import TodoList from '../../src/components/TodoList.vue';
import { startMockServer, stopMockServer } from '../../pacts/MockServer.js';

describe('TodoList.vue test', () => {
    let wrapper = shallowMount(TodoList, {
        data: () => ({
            todoList: [],
            description: "",
        }),
    });

    beforeAll(async () => {
        await startMockServer();
    });
    afterAll(async () => {
        await stopMockServer();
    });

    // Run before to each tests
    beforeEach(() => {
        wrapper = shallowMount(TodoList, {
            data: () => ({
                todoList: [],
                description: "",
            }),
        });
    });

    // Run after to each tests
    afterEach(() => {
        wrapper || wrapper.destroy();
    });

    it('Initializes with correct element and data', async () => {
        expect(wrapper.exists()).toBe(true);
        let warningDiv = wrapper.find("div[test-key='warning']");
        expect(warningDiv.exists()).toBe(true);
        expect(warningDiv.text()).toEqual("List is empty");
        await wrapper.setData({
            todoList: [
                { toDo: 'firstToDo', id: 1 },
                { toDo: 'secondToDo', id: 2 },
            ]
        });
        warningDiv = wrapper.find("div[test-key='warning']");
        expect(warningDiv.exists()).toBe(false);
        const listElement = wrapper.find("[test-key='list']");
        expect(listElement.find("div[id='1']").text()).toEqual("firstToDo");
        expect(listElement.find("div[id='2']").text()).toEqual("secondToDo");
        const descriptionInput = wrapper.find("[test-key='toDoDescription']")
        expect(descriptionInput.exists()).toBe(true);
        expect(descriptionInput.element.value).toEqual("");
        const addButton = wrapper.find("[test-key='addButton']");
        expect(addButton.exists()).toBe(true);
        expect(addButton.text()).toEqual("Add Todo");
    });

    it('Gives the correct reaction to business logic', async () => {
        expect(wrapper.exists()).toBe(true);
        let warningDiv = wrapper.find("div[test-key='warning']");
        expect(warningDiv.exists()).toBe(true);
        const descriptionInput = wrapper.find("[test-key='toDoDescription']")
        expect(descriptionInput.exists()).toBe(true);
        expect(descriptionInput.element.value).toEqual("");
        descriptionInput.setValue("New Todo");
        expect(descriptionInput.element.value).toEqual("New Todo");
        const addButton = wrapper.find("[test-key='addButton']");
        expect(addButton.exists()).toBe(true);
        await addButton.trigger('click');
        wrapper.vm.$nextTick(() => {
            const listElement = wrapper.find("[test-key='list']");
            expect(listElement.exists()).toBe(true);
            let todos = wrapper.find("[test-key='list']").findAll('div');
            expect(todos.at(todos.length - 1).text()).toEqual("New Todo");
        });
    });
});