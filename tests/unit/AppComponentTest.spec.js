import { shallowMount } from '@vue/test-utils';
import App from '../../src/App.vue';
import TodoList from '../../src/components/TodoList.vue';

test('App', () => {
    // Render the component
    const wrapper = shallowMount(App);
    expect(wrapper.findComponent(TodoList).exists()).toBe(true);
});
