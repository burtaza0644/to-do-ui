const { Service } = require('../../src/services');
const { startMockServer, stopMockServer } = require('../../pacts/MockServer.js');
const { describe, before, after, it } = require('mocha');
const assert = require('assert');

describe("Consumer Driven Contract Test", async () => {

    before(async () => {
        await startMockServer()
    });
    after(async () => await stopMockServer());

    describe('When i called UserService Api with valid data', () => {
        it('will receive the list of todos', async () => {
            const response = await Service.User.SetTodo({ description: 'New Todo' });
            assert.ok(!response.error);
            assert.ok(Array.isArray(response.data));
        });
    });

    describe('When i called UserService Api with invalid data', () => {
        it('will receive an object which contains error property equals true and data property equals null', async () => {
            const response = await Service.User.SetTodo({ description: null });
            assert.ok(response.error === true);
            assert.ok(response.data === null);
        });
    });
});