describe("Creating a ToDo", () => {
  it("Displays the ToDo in the list", () => {
    cy.visit("http://34.125.216.15:1018");
    cy.get("[test-key='toDoDescription']").type("New Todo");

    cy.get("[test-key='addButton']").click();

    cy.get("[test-key='toDoDescription']").should("have.value", "");

    cy.get("[test-key='list']").find("div").last().should("have.text","New Todo").end();
  });
});
