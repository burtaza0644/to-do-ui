const { Fetch } = require("../components/Fetch.js");
const TodoApp = require("../config/AppConfig.js");

module.exports = {
  UserService: {
    SetTodo(data) {
      const requestOptions = {
        url : TodoApp.service.user.setTodo,
        method: 'POST',
        data,
      };
      return Fetch(requestOptions);
    },
  }

}

TodoApp.service.user = {
  setTodo: TodoApp.api.user('/User/SetTodo')
}