const axios = require('axios').default;


const Fetch = (requestOptions) => (
    axios({
        ...requestOptions,
        headers: {
            "Content-Type": "application/json"
        },
        responseType:'json'
    }).then(response => {
        const { status , data} = response;
        if (status === 200) {
            return data;
        } else {
            throw new Error('NewError');
        }
    }).then(json => json)
        .catch(() => false)
);

module.exports = { Fetch };