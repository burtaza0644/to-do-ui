const TodoApp = {
    config: {
        /**
       * [GENERAL]
       */
        // eslint-disable-next-line func-names, prefer-arrow-callback, no-unused-vars
        debug: () => {
            try{
                if (/.34.125.216.15:1018./.test(window.location.href)) {
                    return 'test';
                    // eslint-disable-next-line func-names, prefer-arrow-callback, no-unused-vars
                } if (/param/.test(function (param) { })) {
                    return 'debug';
                }
                return 'release';
            }
            catch{
                return 'test';
            }
        },
    },
    api: {
        release: {
            user: 'http://34.125.216.15:1710/api',
        },
        debug: {
            user: 'http://34.125.216.15:1711/api',
        },
        test: {
            user: 'http://34.125.216.15:1711/api',
        },
        user: url => (TodoApp.api[TodoApp.config.debug()].user.concat(url)),
    },
    service: {
        user: {},
    },

}

module.exports = TodoApp;