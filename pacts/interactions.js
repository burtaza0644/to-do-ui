const EXPECTED_BODY = {
    error: false,
    message: "",
    data: [{
        toDo: 'New Todo',
        id: 1,
    }],
}

const EXPECTED_BODY_ONERROR = {
    error: true,
    message: "An error occured",
    data: null,
};

const interactions = {
    validRequest: {
        state: 'Valid Request',
        uponReceiving: 'A Request For Json Data',
        withRequest: {
            method: 'POST',
            path: '/api/User/SetTodo',
            headers: {
                "Content-Type": "application/json"
            },
            body: { description: 'New Todo' },
        },
        willRespondWith: {
            status: 200,
            body: EXPECTED_BODY,
        }
    },

    invalidRequest: {
        state: "Null description",
        uponReceiving: "A Request For Json Data",
        withRequest: {
            method: "POST",
            path: '/api/User/SetTodo',
            headers: {
                "Content-Type": "application/json"
            },
            body: { description: null },
        },
        willRespondWith: {
            status: 200,
            body: EXPECTED_BODY_ONERROR
        }
    }
}

module.exports = { interactions };