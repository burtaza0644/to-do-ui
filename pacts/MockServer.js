const { Pact } = require('@pact-foundation/pact');
const { interactions } = require('./interactions');

const MOCK_SERVER_PORT = 1710;

const provider = new Pact({
    port: MOCK_SERVER_PORT,
    consumer: 'TodoApp',
    provider: 'TodoApi',
    cors: true,
    logLevel: 'error',
});

const MockServer = {
    startMockServer: async () => {
        try {
            await provider.setup();
            Object.keys(interactions).map(async interaction => {
                await provider.addInteraction(interactions[interaction]);
            });
            return;
        } catch {
            return;
        }
    },
    addSpecificInteraction: async interaction => {
        try {
            await provider.addInteraction(interaction);
            return;
        } catch {
            return;
        }
    },
    stopMockServer: async () => {
        try {
            await provider.finalize();
        }
        catch {
            return;
        }
    }
}

module.exports = MockServer;